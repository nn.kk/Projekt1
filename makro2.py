# coding=utf-8
from datetime import datetime
from xlwings import Book

def clear_data():
    # Nazwa arkusza źródłowego
    sheet_name = "Klient"
    # Nazwa arkusza na wyniki
    output = "KlientC"
    # Inicjujemy aktywny skoroszyt, aby inne funkcje wiedziały skąd brać dane
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Book.caller
    wb = Book.caller()
    # Próbujemy usunąć arkusz, ignorujemy błąd, jeśli nie istniał
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Book.sheets
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Sheet.delete
    try:
        wb.sheets[output].delete()
    except:
        pass
    # Tworzymy nowy arkusz o zadanej nazwie
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.main.Sheets.add
    wb.sheets.add(output, after=sheet_name)
    # Wybiera aktywny arkusz (właśnie utworzony)
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.main.Sheets.active
    sheet = wb.sheets.active
    # Wyciągnięcie największego obiektu `Range` zawierającego `A1` ale nie zawierającego pustych pól
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Range.current_region
    table = wb.sheets[sheet_name].range('A1').current_region
    # wyciągnięcie wartości (dane osobowe z kolumny A, daty z B i numery klienta z C)
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Range.options
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Range.value
    [names, dates, ids] = table.options(transpose=True).value
    # wyrzucamy stare nagłówki tabeli
    names = names[1:]
    dates = dates[1:]
    ids = ids[1:]
    # inicjalizacja zmiennych na podzielone dane
    firstnames = []
    surnames = []
    # przechodzimy po wszystkich osobach
    for data in names:
        # dzielimy napisy na spacjach, wyrzucamy puste, powiększamy 1. literę
        parts = [part.capitalize() for part in data.split(" ") if part]
        # zapamiętanie danych
        firstnames.append(" ".join(parts[:-1]))
        surnames.append(parts[-1])
    # zmiana formatu dat na obiekt `datetime`
    dates = [datetime.strptime(data, '%d-%m-%Y') for data in dates]
    starosc=[]
    for data in dates:
        starosc.append(datetime.today().year-data.year)
    # Nagłówek tabeli
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Range.color
    sheet.range('A1:E1').value = ["Imię", "Nazwisko", "Data", "Numer", "Wiek"]
    sheet.range('A1:E1').color = (150, 150, 150)
    # Wpisanie danych do tabeli
    sheet.range('A2').options(transpose=True).value = firstnames
    sheet.range('B2').options(transpose=True).value = surnames
    sheet.range('C2').options(transpose=True).value = dates
    sheet.range('D2').options(transpose=True).value = ids
    sheet.range('E2').options(transpose=True).value = starosc
    # Ustawienie rozmiaru
    #  http://docs.xlwings.org/en/stable/api.html#xlwings.Range.autofit
    sheet.range('A1').current_region.autofit()
def clear_data2():
    sheet_name = "Zakupy"
    output = "ZakupyC"
    wb = Book.caller()
    try:
        wb.sheets[output].delete()
    except:
        pass
    wb.sheets.add(output, after=sheet_name)
    sheet = wb.sheets.active
    table = wb.sheets[sheet_name].range('A1').current_region
    [ids, dates, sizes, costs] = table.options(transpose=True).value
    ids = ids[1:]
    dates = dates[1:]
    sizes = sizes[1:]
    costs=costs[1:]
    dates = [datetime.strptime(data, '%d.%m.%Y') for data in dates]
    format_kasa='''# ##0,00zł'''
    n=len(costs)
    sheet.range((2,4), (n+1,4)).number_format = format_kasa
    sheet.range('A1:D1').value = ["Numer", "Data Zakupu", "Rozmiar koszyka", "Koszt koszyka"]
    sheet.range('F1').value=["Data Ostatnich Zakupów w Sklepie"]
    sheet.range('A1:D1').color = (200, 100, 100)
    sheet.range('F1').color =( 200, 100, 100)
    sheet.range('F2').value=max(dates)
    sheet.range('A2').options(transpose=True).value = ids
    sheet.range('B2').options(transpose=True).value = dates
    sheet.range('C2').options(transpose=True).value = sizes
    sheet.range('D2').options(transpose=True).value = costs
    
    sheet.range('A1:F1').current_region.autofit()
    
def Ostatnie_zakupy_dla_kazdego():
    sheet_name = "ZakupyC"
    output = "OstatnieZakupy"
    wb = Book.caller()
    try:
        wb.sheets[output].delete()
    except:
        pass
    wb.sheets.add(output, after=sheet_name)
    sheet = wb.sheets.active
    table = wb.sheets[sheet_name].range('A1').current_region
    dane=table.value[1:]
    
    lasts={}
    for id, date, size, cost in dane:
        if id not in lasts:
            lasts[id]=(id,date,size,cost)
        else:
            poprzednie=lasts[id]
            if date>poprzednie[1]:
                lasts[id]=(id,date,size,cost)
    
    sheet.range('A1:D1').value = ["Numer", "Data Ostatniego Zakupu","Rozmiar koszyka", "Koszt ostatniego koszyka"]
    sheet.range('A1:D1').color = (100, 100, 200)
    sheet.range('A2').options(transpose=False).value = sorted(lasts.values())
    #sheet.range('B2').options(transpose=True).value = lasts[date]
    
    sheet.range('A1').current_region.autofit()
    sheet.range('B1').current_region.autofit()
    sheet.range('C1').current_region.autofit()
    sheet.range('D1').current_region.autofit()
    
def Razem():
    # sheet_name1="KlientC"
    # sheet_name2="Zakupy"
    # sheet_name3="OstatnieZakupy"
    output = "RaportGłówny"
    wb = Book.caller()
    # sheet = wb.sheets.active
    try:
        wb.sheets[output].delete()
    except:
        pass
    # wb.sheets.add(output, after=sheet_name1)
    # wb.sheets.add(output, after=sheet_name2)
    # wb.sheets.add(output, after=sheet_name3)
    
    sheet = wb.sheets.active
    klient=wb.sheets["KlientC"].range('A1').current_region
    zakupy=wb.sheets["ZakupyC"].range('A1').current_region
    ostatnie_zakupy=wb.sheets["OstatnieZakupy"].range('A1').current_region
    
    ile_kupil={}
    zakup=zakupy.value[1:]
    
    for ids,dates,sizes,costs in zakup:
        if ids not in ile_kupil:
            ile_kupil[ids]=sizes
        else:
            ile_kupil[ids]=ile_kupil[ids]+sizes
    
    kiedy_kupil={}
    ostatni_zakup=ostatnie_zakupy.value[1:]
    for ids, dates, sizes, costs in ostatni_zakup:
        if ids not in kiedy_kupil:
            kiedy_kupil[ids]=(dates, sizes)
            
    wyniki=[]
    klienci=klient.value[1:]
    for firstnames, surnames, dates, ids, wiek in klienci:
        numer, imie, nazwisko=ids, firstnames, surnames
        if ids not in ile_kupil:
            ile=0
        else:
            ile=ile_kupil[ids]
        if ids not in kiedy_kupil:
            kiedy=(None,0)
        else:
            kiedy=kiedy_kupil[ids]
            
        wyniki.append((numer,imie,nazwisko,ile)+kiedy)
        
    sheet.range('A1:F1').value = ["Numer", "Imię","Nazwisko", "Wielkość wszystkich zakupów", "Data ostatnich zakupów", "Rozmiar ostatnich zakupów"]
    sheet.range('A1:F1').color = (100, 100, 200)
    sheet.range('A2').options(transpose=False).value =wyniki
    
    sheet.range('A1').current_region.autofit()
    sheet.range('B1').current_region.autofit()
    sheet.range('C1').current_region.autofit()
    sheet.range('D1').current_region.autofit()
    sheet.range('E1').current_region.autofit()
    sheet.range('F1').current_region.autofit()
    